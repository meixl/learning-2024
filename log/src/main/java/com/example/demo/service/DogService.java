package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Dog;

public interface DogService {

    List<Dog> list();

}
