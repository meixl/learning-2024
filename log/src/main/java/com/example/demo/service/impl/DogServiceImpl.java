package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.model.Dog;
import com.example.demo.service.DogService;

@Service
public class DogServiceImpl implements DogService {

    private static final Logger log = LoggerFactory.getLogger(DogServiceImpl.class);

    @Override
    public List<Dog> list() {

        log.debug("list");

        List<Dog> dogs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Dog dog = new Dog().setId(i);
            dogs.add(dog);
        }
        return dogs;
    }

}
