package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Dog;
import com.example.demo.service.DogService;

@RestController
@RequestMapping("/dog")
public class DogController {

    @Autowired
    private DogService          dogService;

    private static final Logger log = LoggerFactory.getLogger(DogController.class);

    @RequestMapping("/list")
    public String list() {
        log.info("list. time: {}", LocalDateTime.now());
        List<Dog> list = dogService.list();
        return list.toString();
    }

}
