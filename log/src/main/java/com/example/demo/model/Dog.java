package com.example.demo.model;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Dog {

    private Integer    id;

    private String     name;

    private Integer    age;

    private BigDecimal weight;

}
