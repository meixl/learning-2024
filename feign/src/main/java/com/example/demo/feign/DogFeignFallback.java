package com.example.demo.feign;

import org.springframework.stereotype.Component;

import com.example.demo.model.request.DogRequest;

@Component
public class DogFeignFallback implements DogFeign {

    @Override
    public String get(DogRequest request) {
        return "fallback get 1";
    }

    @Override
    public String get(Integer id) {
        return "fallback get 2";
    }

    @Override
    public String post(DogRequest request) {
        return "fallback post";
    }

}
