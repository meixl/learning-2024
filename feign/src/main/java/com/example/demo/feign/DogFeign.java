package com.example.demo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.request.DogRequest;

@FeignClient(name = "mei-dog", path = "/dog", fallback = DogFeignFallback.class)
public interface DogFeign {

    @GetMapping("/get")
    String get(@SpringQueryMap DogRequest request);

    @GetMapping("/{id}")
    String get(@PathVariable Integer id);

    @PostMapping("/post")
    String post(@RequestBody DogRequest request);

}
