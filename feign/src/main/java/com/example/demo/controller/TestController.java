package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.feign.DogFeign;
import com.example.demo.model.request.DogRequest;

import jakarta.annotation.Resource;

@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private DogFeign dogFeign;

    @GetMapping("/get")
    public String get(DogRequest request) {

        return dogFeign.get(request);

    }

    @GetMapping("/{id}")
    public String get(@PathVariable Integer id) {

        return dogFeign.get(id);

    }

    @PostMapping("/post")
    public String post(@RequestBody DogRequest request) {

        String post = dogFeign.post(request);

        return post;

    }

}
