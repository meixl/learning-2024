package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Dog;
import com.example.demo.model.request.DogRequest;

@RestController
@RequestMapping("/dog")
@RefreshScope
public class DogController {

    @Value("${aa.bb}")
    private Integer num;

    @Autowired
    Environment     environment;

    @GetMapping("/get")
    public String get(DogRequest request) {

        Dog dog = new Dog() //
            .setId(request.getId()) //
            .setName(request.getName()) //
            .setTime(request.getTime()) //
            .setSize(request.getSize()) //
        ;

        System.out.println(1);

        return "from idea: " + dog.toString() + ", from idea num: " + num;

    }

    @GetMapping("/{id}")
    public String get(@PathVariable Integer id) {

        Dog dog = new Dog() //
            .setId(id) //
        ;

        System.out.println(2);

        return "from idea: " + dog.toString();

    }

    @PostMapping("/post")
    public String post(@RequestBody DogRequest request) {

        Dog dog = new Dog() //
            .setId(request.getId()) //
            .setName(request.getName()) //
            .setTime(request.getTime()) //
            .setSize(request.getSize()) //
        ;

        System.out.println(3);

        String port = environment.getProperty("local.server.port");

        return "from idea: " + dog.toString() + ", port: " + port;

    }

}
