package com.example.demo.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Dog {

    private Integer       id;

    private String        name;

    private LocalDateTime time;

    private BigDecimal    size;

}
